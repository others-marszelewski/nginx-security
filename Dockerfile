FROM nginx

COPY nginx.conf /etc/nginx/nginx.conf

VOLUME /etc/nginx

EXPOSE 8080
